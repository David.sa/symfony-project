<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ArticleType;
use App\Form\VideoType;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function seeArticles(ArticleRepository $repo)
    {
        $articles = $repo->findAll();
        return $this->render('blog/blog.html.twig', [
            'articles' => $articles
        ]);
    }
    /**
     * @Route("/blog/{id}", name="show-article")
     */
    public function showArticle(Article $article)
    {
        return $this->render('blog/show.html.twig', [
            'article' => $article
        ]);
    }
    /**
     * @Route("/create_article", name="create_article")
     */
    public function createArticle(Request $request, ObjectManager $manager)
    {

        $article = new article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setDate(new \datetime());
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog');
        }

        return $this->render('blog/create.html.twig', [
            "formArticle" => $form->createView()
        ]);
    }
    /**
     * @Route("/supprime/article/{article}", name="supprime")
     */
    public function remove(Article $article, ObjectManager $manager)
    {
        $manager->remove($article);
        $manager->flush();
        return $this->redirectToRoute('blog');
    }
    /**
     * @Route("/modifie/{article}", name="modifie")
     */
    public function modifie(Request $request, Article $article, ObjectManager $manager)
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { 
            dump($article);
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog');
        }
        return $this->render("blog/create.html.twig", [
            "formArticle" => $form->createView()
        ]);
    }
}
