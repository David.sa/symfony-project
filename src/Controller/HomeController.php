<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Form\UserType;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
        public function home()
    {
            return $this->render('home/index.html.twig');
    }
    /**
     * @Route("home/DavidS", name="DavidS")
     */
    public function bio()
    {
        return $this->render('home/DavidS.html.twig');
    }
     /**
     * @Route("/profile", name="profile")
     */
    public function profile(){

        return $this->render('home/profile.html.twig', [
            
        ]);
    }
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $auth){
        $errors = $auth->getLastAuthenticationError();
        $username = $auth->getLastUsername();

        return $this->render('home/login.html.twig', [
            'errors' => $errors,
            'username' => $username
        ]);
    }
     /**
     * @Route("/register", name="register")
     */
    public function register(Request $request,
                            UserPasswordEncoderInterface $encoder, 
                            ObjectManager $manager) {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $pass = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pass);
            
            $manager->persist($user);
            $manager->flush();
        
            // return $this->redirectToRoute('login');
        }

        return $this->render('home/register.html.twig', [
            'form' => $form->createView()    
        
        ]);
    }

}
