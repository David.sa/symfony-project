<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Video;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use App\Form\TextType;

class VideoController extends AbstractController
{
    /**
     * @Route("/video", name="video")
     */
    public function allVideos(VideoRepository $repo)
    {
        $videos = $repo->findAll();
        return $this->render('video/video.html.twig', [
            'videos' => $videos
        ]);
    }
     /**
     * @Route("/add_video", name="add_video")
     */
    public function addVideo(Request $request, ObjectManager $manager){

        $video = new video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($video);
            $manager->persist($video);
            $manager->flush();

            return $this->redirectToRoute('video');
        }
                   
        return $this->render('video/addvideo.html.twig', [
        "formVideo" => $form->createView()
        ]);
    }
    /**
     * @Route("/video/{id}", name="showvideo")
     */
    public function showVideo(Video $video)
    {
        return $this->render('video/showvideo.html.twig', [
            'video' => $video
        ]);
    }
     /**
     * @Route("/suppr/video/{video}", name="suppr")
     */
    public function remove(Video $video, ObjectManager $manager)
    {
        $manager->remove($video);
        $manager->flush();
        return $this->redirectToRoute('video');
    }
}
